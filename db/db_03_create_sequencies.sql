\c stateproject

CREATE SEQUENCE stateproject.public.seq_roles START 1;
CREATE SEQUENCE stateproject.public.seq_users START 1;
CREATE SEQUENCE stateproject.public.seq_categories START 1;
CREATE SEQUENCE stateproject.public.seq_products START 1;
CREATE SEQUENCE stateproject.public.seq_market_types START 1;
CREATE SEQUENCE stateproject.public.seq_markets START 1;
CREATE SEQUENCE stateproject.public.seq_bid_statuses START 1;
CREATE SEQUENCE stateproject.public.seq_bids START 1;
