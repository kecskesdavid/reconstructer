package davidkecskes.stateproject.jpa;
// Generated Jun 1, 2021, 7:26:21 PM by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * VAuthorizations generated by hbm2java
 */
@Entity
@Table(name="v_authorizations"
)
public class VAuthorizations  implements java.io.Serializable {


     private VAuthorizationsId id;

    public VAuthorizations() {
    }

    public VAuthorizations(VAuthorizationsId id) {
       this.id = id;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="roleId", column=@Column(name="role_id") ), 
        @AttributeOverride(name="roleName", column=@Column(name="role_name", length=128) ), 
        @AttributeOverride(name="userId", column=@Column(name="user_id") ), 
        @AttributeOverride(name="userName", column=@Column(name="user_name", length=128) ) } )
    public VAuthorizationsId getId() {
        return this.id;
    }
    
    public void setId(VAuthorizationsId id) {
        this.id = id;
    }




}


